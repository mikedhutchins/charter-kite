import * as React from 'react' 
import Button from './components/button/button.component';
import Dialog from './components/dialog/dialog.component';
import TextControl from './components/text-control/text-control.component';
import './styles.scss'

const Package: React.FC = () => (
  <div className="package">
    <h2>Do cool stuff</h2>
  </div>
)

const library = () => { 
  return { 
    Package: Package,
    Dialog: Dialog,
    Button: Button,
    TextControl: TextControl
  };
};

export default library();
