import * as React from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import './dialog.scoped.scss';

type DialogProps = { 
    icon?: React.ReactChild,
    title?: React.ReactChild | string,
    children: React.ReactChild | React.ReactChild[],
    className?: string,
    visible?: Boolean,
    onClose?: Function,
    actions?: JSX.Element[]
}

const Dialog: React.FC<DialogProps> = ({ 
    icon,
    title,
    children,
    actions,
    visible,
    onClose
}: DialogProps) => { 

    const [isVisible, setIsVisible] = React.useState(visible);
    
    const setInvisible = () => { 
        if (onClose) { 
            onClose();
        }
        setIsVisible(false);
    }

    const cancelClick = (event: React.SyntheticEvent) => {
        event.stopPropagation();
        event.preventDefault();
    };

    React.useEffect(() => setIsVisible(visible), [visible]);

    const defaultAction = <div><button onClick={setInvisible}>Ok</button></div>;
    
    return <>{isVisible && <div className="dialog-outer" onClick={setInvisible}>
            <div className="dialog" onClick={cancelClick}>
                <div className="dialog-title-panel">
                    {icon && <div className="dialog-icon">{icon}</div>}
                    <div className="dialog-title">
                        {title && title}
                    </div>
                    <div className="dialog-close" onClick={setInvisible}>
                        <AiOutlineClose />
                    </div>
                </div>
                <div className="dialog-body">
                    {children}
                </div>
                <div className="shim">
                    
                </div>
                <div className="dialog-actions-panel">
                    {actions ? actions.map(action => <div key={Math.random()}>{action}</div>) : defaultAction}
                </div>
        </div>
    </div>}
    </>;
};

export default Dialog;