import * as React from 'react';
import clsx from 'clsx';
import './button.scoped.scss';
//import { useState } from 'react';

export type ButtonVariants = 'primary' | 'default';

type ButtonProps = {
    children: React.ReactChild | React.ReactChild[],
    variant?: ButtonVariants,
    onClick?: (_event: React.SyntheticEvent) => void,
    className?: string,
    href?: string,
    target?: string
}


export const Button: React.FC<ButtonProps> = ({ 
    children,
    variant,
    onClick,
    className,
    href,
    target
}) => { 
    let _onClick = onClick ? onClick : (_event: React.SyntheticEvent) => { };

    return <>
        {href ? 
            <a className={clsx([
                'kite-btn', 
                variant === 'primary' ? 'kite-btn-primary' : 'kite-btn-default', 
                className
            ])}
                href={href}
                target={target && '_self'}
                onClick={_onClick}
            >
                {children}
            </a> :
            <button className={clsx([
                'kite-btn', 
                variant === 'primary' ? 'kite-btn-primary' : 'kite-btn-default', 
                className
            ])}
            onClick={_onClick}
            >{children}
                </button>}
    </>
};

export default Button;