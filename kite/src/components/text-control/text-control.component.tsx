import * as React from 'react' 
import './text-control.scss';

type TextVariants = "multiline" | "single";
type TextTypes = "text" | "number" | "date";

type TextControlProps = { 
    value?: string,
    id?: string,
    name?: string,
    type?: TextTypes,
    className?: string,
    onChange: (event: string) => void,
    label: React.ReactChild,
    placeholder?: string,
    variant: TextVariants
};

const TextControl: React.FC<TextControlProps> = ({
    value, 
    id, 
    name,
    className,
    onChange,
    type,
    label,
    placeholder,
    variant
}) => { 
    return (
        <div className="text-control-form">
            <label>{label}</label>
            {variant === 'single' 
                ? <input 
                    type={type}
                    id={id}
                    name={name}
                    placeholder={placeholder}
                    className={className}
                    value={value} 
                    onChange={(e) => onChange(e.currentTarget.value)} />
                : <textarea
                    id={id}
                    name={name}
                    placeholder={placeholder}
                    className={className}
                    rows={1}
                    value={value} onChange={(e) => onChange(e.currentTarget.value)}
                    />}
        </div>
    )
};

export default TextControl;

