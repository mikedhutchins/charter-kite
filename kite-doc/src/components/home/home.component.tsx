import React from 'react';

const Home = () => { 
    return <div>
        <h2>Welcome to the Kite Design Library</h2>
        <h3>Select a component to learn more</h3>
    </div>
};

export default Home;