import React, { useState } from 'react';
import PlayPanel from '../play-panel/play-panel.component';
import ComponentPanel from '../component-panel/component-panel.component';
import ApiProperty from '../api-property/api-property.component';
import Kite from 'kite';
import './button.scss';


const ButtonPlay: React.FC = () => { 
    const [variant, setVariant] = useState('primary');
    const [title, setTitle] = useState('Click me, I\'m a Button');


    const controlPanel = <div className="button-panel">
        <Kite.TextControl variant="single" value={title} onChange={(e) => setTitle(e)} label="Button Label" />
        <div className="cell">
            <div className="text-control-form">
                <label>Switch Variant</label>
                <Kite.Button variant={"primary"} onClick={e => setVariant('primary')}>Primary</Kite.Button>
                <Kite.Button variant={"default"} onClick={e => setVariant('default')}>Default</Kite.Button>
            </div>
        </div>
    </div>;

    const codePanel = <>
    </>;

    const componentApi = <>
        <ApiProperty name={"label"} type={"string"} description={"The Call to Action of a button"}></ApiProperty>
        <ApiProperty name={"variant"} type={"enum {primary | default}"} description={"The variant display"}></ApiProperty>
    </>;

    const description = <>
        <h4>The Button</h4>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse libero officiis tenetur corrupti! Suscipit mollitia beatae nulla eum quia nam illo ex, dolorum unde nemo! Enim non id ab iure cum velit doloremque eum illum?
    </>
    return <PlayPanel title={"Component: Button"} description={description}>
    <ComponentPanel 
        controlPanel={controlPanel} 
        codePanel={codePanel}
        componentApi={componentApi}>
        {variant === 'primary' ? <Kite.Button 
           variant="primary">{title}</Kite.Button> : <Kite.Button
           variant="default">{title}</Kite.Button>}
    </ComponentPanel>
</PlayPanel>;
};

export default ButtonPlay;