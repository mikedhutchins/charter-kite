import React from 'react';
import PlayPanel from '../play-panel/play-panel.component';
import ComponentPanel from '../component-panel/component-panel.component';
import Kite from 'kite';
import { useState } from 'react';
import './text-control.scss';

const TextControlPlay: React.FC = () => { 
    const [value, setValue] = useState('Text Value');
    const description = <>
        <h4>Everyone needs a text control</h4>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, beatae sit? Quasi molestiae laudantium aut laboriosam ab minima velit aliquid, similique reiciendis maiores voluptates, omnis, voluptatum magni! Labore, atque quidem?
    </>

    const controlPanel = <div className="text-form-control">
        
    </div>;

    const codePanel = <></>;
    return <PlayPanel title="Component: TextControl" description={description}>
        <ComponentPanel controlPanel={controlPanel} codePanel={codePanel}>
            <Kite.TextControl variant="single" value={value} onChange={(e) => setValue(e)} label="Button Label" />
        </ComponentPanel>
    </PlayPanel>
};

export default TextControlPlay;