import React from 'react';
import './api-property.scss';

export type ApiPropertyProps = {
    name: string,
    type: string,
    description: string
};

const ApiProperty: React.FC<ApiPropertyProps> = ({
    name,
    type,
    description
}) => { 
    return <div className="api-property">
        <div className="name">{name}</div>
        <div className="type">{type}</div>
        <div className="description">{description}</div>
    </div>
};

export default ApiProperty;
