import React from 'react';
import './component-panel.scss';
import ApiProperty from '../api-property/api-property.component';

type ComponentPanelProps = {
    controlPanel: React.ReactChild | React.ReactChild[],
    children: React.ReactChild | React.ReactChild[],
    codePanel: React.ReactChild | React.ReactChild[],
    componentApi?: React.ReactChild | React.ReactChild[]
};

const ComponentPanel: React.FC<ComponentPanelProps> = ({
    controlPanel,
    codePanel,
    children,
    componentApi
}) => { 
    return <div>
        <div className="main-panel">
            <h4>Component</h4>
            {children}
        </div>
        <div className="control-panel">
            <h4>Knobs</h4>
            {controlPanel}
        </div>
        <div className="code-panel">
            <h4>Code</h4>
            {codePanel}
        </div>
        {componentApi && <div className="api-panel">
            <h4>API</h4>
            <ApiProperty name={"NAME"} type={"TYPE"} description={"DESCRIPTION"} />
            {componentApi}
        </div>}
    </div>;
};

export default ComponentPanel;