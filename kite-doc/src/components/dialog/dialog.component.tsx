import React, { useState } from 'react';
import PlayPanel from '../play-panel/play-panel.component';
import ComponentPanel from '../component-panel/component-panel.component';
import ApiProperty from '../api-property/api-property.component';
import Kite from 'kite';
import './dialog.scss';

const DialogPlay: React.FC = () => { 
    const [title, setTitle] = useState('I\'m a Dialog');
    const [body, setBody] = useState('Dialog content');
    const [showModal, setShowModal] = useState(false);
    const description = <>
        <h4>The Dialog / Modal interstitial component.</h4>
        <br />
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugiat beatae, vero eum possimus cupiditate harum eius consequuntur voluptatum, iure est sapiente pariatur corrupti esse dignissimos modi saepe tempore tenetur veritatis.
    </>;

    const controlPanel = <div className="dialog-panel">
        <Kite.TextControl 
            type="text" 
            label="Title" 
            value={title}
            variant="single"
            onChange={(e) => setTitle(e)}></Kite.TextControl>
        <Kite.TextControl 
            type="text" 
            label="Content of Modal" 
            value={body}
            variant="multiline"
            onChange={(e) => setBody(e)}></Kite.TextControl>
    </div>;

const componentApi = <>
    <ApiProperty name={"icon"} type={"string | React.ReactChild"} description={"Icon - If none provided, nothing will be rendered."}></ApiProperty>
    <ApiProperty name={"title"} type={"string | React.ReactChild"} description={"The Title"}></ApiProperty>
    <ApiProperty name={"content"} type={"string | React.ReactChild"} description={"The Content / Components to be loaded."}></ApiProperty>
    <ApiProperty name={"visible"} type={"boolean"} description={"Toggles the Dialog open or closed."}></ApiProperty>
    <ApiProperty name={"actions"} type={"JSX.Element[]"} description={"A collection of actions (buttons).  If none are provided, a single 'OK' action will render.  That 'OK' button will close the Dialog."}></ApiProperty>
    <ApiProperty name={"onClose"} type={"Function () => void"} description={"Your event handler to notify your app when the user has closed the dialog internally."}></ApiProperty>
    </>;

    const codePanel = <>
    </>;
    return (<PlayPanel title={"Component: Dialog"} description={description}>
        <ComponentPanel 
            controlPanel={controlPanel} 
            codePanel={codePanel}
            componentApi={componentApi}>
            <Kite.Button 
               variant="primary"
                onClick={(e) => setShowModal(true)}>Click Here for a Modal</Kite.Button>
            <Kite.Dialog 
                title={<span dangerouslySetInnerHTML={{ __html: title }} />} 
                visible={showModal}
                onClose={() => setShowModal(false)}
                >
                <span dangerouslySetInnerHTML={{__html: body}}></span>
            </Kite.Dialog>

        </ComponentPanel>
    </PlayPanel>);
};

export default DialogPlay;