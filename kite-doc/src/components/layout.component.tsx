import React from 'react';
import './layout.scss';

type LayoutProps = {  
    navigation: React.ReactChild | React.ReactChild[],
    children: React.ReactChild | React.ReactChild[],
    title: React.ReactChild | React.ReactChild[]    
}

const Layout: React.FC<LayoutProps> = ({
    navigation, 
    children,
    title
}) => { 
    return <div className="layout">
        <header>
            <h1>
            {title}
            </h1>
        </header>
        <div className="layout-inner-panel">
            <div className="layout-navigation-panel">
                {navigation}
            </div>
            <div className="layout-body-panel">
                {children}
            </div>
        </div>
    </div>
};

export default Layout;