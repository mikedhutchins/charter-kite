import React from 'react';

type PlayPanelProps = {
    title: React.ReactChild | React.ReactChild[],
    children: React.ReactChild | React.ReactChild[],
    description: React.ReactChild | React.ReactChild[],
};

const PlayPanel: React.FC<PlayPanelProps> = ({
    title,
    children,
    description
}) => {
    return <>
        <h2>{title}</h2>
        <h3>Description</h3>
        <p>
            {description}
        </p>
        {children}
    </>;
};

export default PlayPanel;
