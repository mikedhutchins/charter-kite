import React from 'react';
import { Link } from 'react-router-dom';
import './navigation.scss';

const Navigation: React.FC = () => { 
    return <>
        <Link to="/" className="nav-item">Home</Link>
        <hr />        
        <Link to="/button" className="nav-item">Button</Link>
        <Link to="/dialog" className="nav-item">Dialog</Link>
        <Link to="/text-control" className="nav-item">Text Control</Link>
    </>
};

export default Navigation;