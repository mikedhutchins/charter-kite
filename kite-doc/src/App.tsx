import React, { useState } from 'react';
import Kite from 'kite';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Layout from './components/layout.component';
import Home from './components/home/home.component';

import './App.scss';
import Navigation from './components/navigation/navigation.component';
import DialogPlay from './components/dialog/dialog.component';
import ButtonPlay from './components/button/button.component';
import TextControlPlay from './components/text-control/text-control.component';

const App = () => {
  return (<>
      <Router>
        <Layout title={"Kite UI / UX Design System v0.0.1"} 
          navigation={<Navigation />}>
            <Route path="/" exact component={Home}></Route>
            <Route path="/dialog" exact component={DialogPlay}></Route>
            <Route path="/button" exact component={ButtonPlay}></Route>
            <Route path="/text-control" exact component={TextControlPlay}></Route>
        </Layout>
      </Router>
    </>
  );
}

export default App;
